const express = require("express");
const path = require("path");
const PORT = process.env.PORT || 5000;
const ccxt = require("ccxt");
var cookieParser = require("cookie-parser");
var session = require("express-session");
var cors = require("cors");
var app = express();
var bodyParser = require("body-parser");
const Zenfuse = require("./helpers/zenfuse");
require("dotenv").config();
var router = express.Router();
// Тестирование шаблонизатора
app.set("view engine", "pug");
app.set("views", "./views");
const successMessage = "Сделка прошлам успешно";

app
  .use(cookieParser())
  .use(cors())
  .set("trust proxy", 1)
  .use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: true,
      cookie: { secure: false }
    })
  )
  .get("/", (req, res) => res.render("pages/index"))
  .listen(PORT, () => console.log(`Listening on ${PORT}`));

app.use(bodyParser.json());

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// Получить названия всех бирж
app.get("/exchanges", async function(req, res) {
  let z = new Zenfuse();
  res.json(await z.getAllExchanges());
});

// Получить все пары и значения
app.get("/markets", async function(req, res) {
  let z = new Zenfuse();
  res.json(
    await z.getBinanceMarkets(
      process.env.BINANCE,
      process.env.API_KEY,
      process.env.SECRET_KEY
    )
  );
});

// Получение данных о паре из API :id - btcusdt
app.get("/markets/:pairId", async function(req, res) {
  let z = new Zenfuse();
  res.send(
    await z.getBinanceMarketById(
      req.params.pairId,
      process.env.BINANCE,
      process.env.API_KEY,
      process.env.SECRET_KEY
    )
  );
});

// Получить все сделки по валюте - история торгов
app.post("/my-trades", async function(req, res) {
  let z = new Zenfuse();
  try {
    res.json(
      await z.getAllMyTrades(
        req.body.pair,
        process.env.BINANCE,
        process.env.API_KEY,
        process.env.SECRET_KEY
      )
    );
  } catch (e) {
    res.send(e.message);
  }
});

// Баланс аккаунта
app.get("/balance", async function(req, res) {
  let z = new Zenfuse();
  res.json(
    await z.getBinanceBalance(
      process.env.BINANCE,
      process.env.API_KEY,
      process.env.SECRET_KEY
    )
  );
});

// Сделка на покупку: пара, количество
app.post("/buy", async function(req, res) {
  let z = new Zenfuse();
  try {
    await z.buyBinanceByCurrentPrice(
      req.body.pair,
      req.body.amount,
      process.env.BINANCE,
      process.env.API_KEY,
      process.env.SECRET_KEY
    );
    res.send(successMessage);
  } catch (e) {
    res.send(e.message);
  }
});

// Сделка на продажу: пара, количество
app.post("/sell", async function(req, res) {
  let z = new Zenfuse();
  try {
    await z.sellBinanceByCurrentPrice(
      req.body.pair,
      req.body.amount,
      process.env.BINANCE,
      process.env.API_KEY,
      process.env.SECRET_KEY
    );
    // res.send(successMessage);
  } catch (e) {
    res.send(e.message);
  }
});

// Получаем ордеры по паре
app.get("/opened-orders/:firstPairId/:secondPairId", async function(req, res) {
  let z = new Zenfuse();
  try {
    res.json(
      await z.getMyOpenedOrdersForThePair(
        req.params.firstPairId,
        req.params.secondPairId,
        process.env.BINANCE,
        process.env.API_KEY,
        process.env.SECRET_KEY
      )
    );
  } catch (e) {
    res.send(e.message);
  }
});

// Отмена ордера по паре и id
app.post("/cancel-orders", async function(req, res) {
  let z = new Zenfuse();
  try {
    res.json(
      await z.closeOrderById(
        req.body.id,
        `${req.body.firstPairId}/${req.body.secondPairId}`,
        process.env.BINANCE,
        process.env.API_KEY,
        process.env.SECRET_KEY
      )
    );
    // res.send(successMessage);
  } catch (e) {
    res.send(e.message);
  }
});

app.get("/cancel-all-orders", async function(req, res) {
  let z = new Zenfuse();
  try {
    // Получить все валюты
    let currencies = await z.getBinanceMarkets(
      process.env.BINANCE,
      process.env.API_KEY,
      process.env.SECRET_KEY
    );
    // Получить все открытые сделки для всех валют
    let markets = [];
    for (key in currencies) {
      // console.log("loading");
      // Добавляем в массив все сделки
      if (key == "BTC/USDT") {
        let res = await z.getAllMyTrades(
          key,
          process.env.BINANCE,
          process.env.API_KEY,
          process.env.SECRET_KEY
        );
        markets.push(res);
      }
    }
    let markThen = Promise.all(markets);
    // res.json(markThen);
    // console.log(markets);
    // let markets = await z.getAllMyTrades(
    //   "BTC/USDT",
    //   process.env.BINANCE,
    //   process.env.API_KEY,
    //   process.env.SECRET_KEY
    // );
    let openedOrdersMarkets = [];
    // res.json(markets[0]);
    // Пробегаем только по первому (костыль)!!!!
    for (const el of markets[0]) {
      if (el.info.status !== "FILLED") {
        openedOrdersMarkets.push(el);
      }
      // el.filter(function(e) {
      //   return e.info.status == "FILLED";
      // }).push(openedOrdersMarkets);
    }
    // res.json(openedOrdersMarkets.length);
    // Закрыть их
    if (openedOrdersMarkets.length !== 0) {
      for (const openedOrder of openedOrdersMarkets) {
        await z.closeOrderById(
          openedOrder.id,
          openedOrder.symbol,
          process.env.BINANCE,
          process.env.API_KEY,
          process.env.SECRET_KEY
        );
        res.send(successMessage);
      }
    } else {
      res.send("У вас нет открытых сделок");
    }
  } catch (e) {
    res.send(e.message);
  }
});

// Login api
app.post("/login", async function(req, res) {
  if (
    req.body.username === "zenfuse" &&
    req.body.password === "zenfusewillfuckeverybody"
  ) {
    req.session.authorized = true;
    req.session.username = req.body.username;
    console.log(req.session);
    res.send(true);
    // res.send(
    //   "Thor is here! " + JSON.stringify(req.body) + JSON.stringify(req.session)
    // );
  }
  // res.send(JSON.stringify(req.body) + JSON.stringify(req.session));
});

// Logout
app.post("/logout", async function(req, res) {
  if (req.session.authorized) {
    req.session.authorized = false;
    req.session.username = null;
    console.log("You logged out.");
    res.send(true);
    // res.send("Thor is here! " + JSON.stringify(req.session));
  } else {
    res.send(req.session.authorized);
  }
  // res.send(JSON.stringify(req.body) + JSON.stringify(req.session));
});

// Функция промежуточной обработки для ошибок
app.get((err, req, res, next) => {
  console.log(err.stack);
});
