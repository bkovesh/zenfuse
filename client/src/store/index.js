import React from "react";
import useGlobalHook from "use-global-hook";
import * as actions from "../actions";

const initialState = {
  status: "INITIAL",
  //user
  email: '',
  session: {logged:false,userId:null,sessionId:null},
  // ui
  actLogin: 'Login',
  actTrading: 'Balances',
  showExchanges: [],
  // ccxt
  symbols: [],
  htmlSymbols: [],
  htmlExchanges: [],
  balances: {},
  trades: {},
  markets: {},
  myExchanges: [],
  exchanges: [],
  exchangesInfo: {},
  currenciesInfo: {},
  rebalance: {},
};

const useGlobal = useGlobalHook(React, initialState, actions);
export default useGlobal;
