import React, {useEffect, useState} from 'react';
import { Modal, Input, Form, Icon, Button, Select, message } from 'antd';
import useGlobal from "store";

const { Option } = Select;

const ModalAddExchange = (props) => {
  const [globalState, globalActions] = useGlobal();
  const {addExchange,setVar} = globalActions.actions;
  const {exchanges,name,apiKey,secretKey,exchangesInfo} = globalState;
  const [selectedItems, setSelectedItems] = useState([]);
  const [filteredItems, setFilteredItems] = useState([]);

  useEffect(() => {
    setFilteredItems(exchanges)
  },[exchanges]);

  const handleChange = selectedItems => {
    setSelectedItems(selectedItems);
    setFilteredItems(exchanges.filter(o => !selectedItems.includes(o)))
    setVar('name',selectedItems);
  };

  const add = async () => {
    let exchange = {name, apiKey, secretKey};
    let result = await addExchange(exchange);
    if (result&&result.success===true) message.success('Exchange added')
    if (result&&result.success===false) message.warning(result.message)
    return;
  }

  return (
    <Modal
    // title="Add Exchange"
    visible={props.visible}
    onOk={props.handleOk}
    onCancel={props.handleCancel}
    footer={null}
    closeIcon={<Icon type="close-circle" size="large" />}
    style={{margin:'auto'}}
    >

      <Form className="login-form" style={{padding:'40px 20px'}}>
        <Form.Item style={{textAlign:'center'}}>
          <h3>Add Exchange</h3>
        </Form.Item>
        <Form.Item>



          <Select
          showSearch
          // style={{ width: 200, margin:10 }}
          size="large"
          placeholder="Exchange"
          optionFilterProp="children"
          onChange={handleChange}
          filterOption={(input, option) =>
            option.props.children[1].toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
          >
            {exchanges && exchanges.map((item,i) => {
              return(
              <Option key={`option-${item}-${i}`} value={item}>
                {exchangesInfo&&exchangesInfo[item]&&<img src={exchangesInfo[item].urls.logo} style={{padding:'0 5px'}}/>}
                {item}
              </Option>
              )
            })}
          </Select>


          {/*<Select*/}
          {/*size="large"*/}
          {/*placeholder="Exchange"*/}
          {/*value={selectedItems}*/}
          {/*onChange={handleChange}*/}
          {/*>*/}
            {/*{filteredItems&&filteredItems.map(item => (*/}
              {/*<Select.Option key={item} value={item}>*/}
                {/*{exchangesInfo&&exchangesInfo[item]&&<img src={exchangesInfo[item].urls.logo} style={{padding:'0 5px'}}/>}*/}
                {/*{item}*/}
              {/*</Select.Option>*/}
            {/*))}*/}
          {/*</Select>*/}
        </Form.Item>
        <Form.Item>
          <Input
          placeholder="API key"
          style={{height:'54px'}}
          onChange={(e) => setVar('apiKey',e.target.value)}
          />
        </Form.Item>
        <Form.Item>
          <Input
          placeholder="Secret key"
          style={{height:'54px'}}
          onChange={(e) => setVar('secretKey',e.target.value)}
          />
        </Form.Item>
        <Form.Item>
          <Button
          key="submit"
          size="large" block type="primary"
          onClick={async ()=>{
            await add();
            props.handleOk();
          }}
          style={{height:'54px'}}
          >
            Add
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default ModalAddExchange;
