import React, {useEffect} from 'react';
import { Menu, Avatar } from 'antd';
import { Link } from 'react-router-dom';
import useGlobal from 'store';

const MenuMain = () => {
  const [globalState, globalActions] = useGlobal();
  const { logout, getFromStorage } = globalActions.actions;
  const { logged, email, sessionId } = globalState;

  const getSession = () => {
    return getFromStorage('session');
  }


  useEffect(() => {
    renderMenu()
  },[logged]);


  const htmlMenuLogged = (
    <Menu
    mode="horizontal"
    theme="dark"
    selectable={false}
    style={{
      position: 'fixed',
      zIndex: 1,
      width: '100%',
      background: '#0B1924',
      boxShadow: '0px 3px 7px rgba(26, 26, 30, 0.15)',
    }}
    >
      <Menu.Item key="main" style={{float: 'left'}}>
        <Link to="/">
          <Avatar
          size="small"
          src="https://zenfuse.io/img/zenfuse_logo.svg"
          style={{margin: '10px'}}
          />
          <strong style={{color: 'white'}}>zenfuse</strong>
        </Link>
      </Menu.Item>
      <Menu.Item key="logout" style={{float:'right'}}>
        <Link
        to="/"
        onClick={()=>logout({sessionId:sessionId})}
        >
          Log out
        </Link>
      </Menu.Item>
      <Menu.Item key="settings" style={{float: 'right'}}>
        <Link to="/settings">{getSession()&&getSession().email}</Link>
      </Menu.Item>
      <Menu.Item key="calendar" style={{float: 'right'}}>
        <Link to="/calendar">Calendar</Link>
      </Menu.Item>
      <Menu.Item key="cabinet" style={{float: 'right'}}>
        <Link to="/cabinet">Cabinet</Link>
      </Menu.Item>
      <Menu.Item key="community" style={{float: 'right'}}>
        <a href="https://t.me/zenfuse_en" target="_blank" rel="noopener noreferrer">Join Community</a>
      </Menu.Item>
      <Menu.Item key="landing" style={{float: 'right'}}>
        <a href="https://zenfuse.io" target="_blank" rel="noopener noreferrer">Go to Landing Page</a>
      </Menu.Item>
    </Menu>
  )


  const htmlMenuNotLogged = (
    <Menu
    mode="horizontal"
    theme="dark"
    selectable={false}
    style={{
      position: 'fixed',
      zIndex: 1,
      width: '100%',
      background: '#0B1924',
      boxShadow: '0px 3px 7px rgba(26, 26, 30, 0.15)',
    }}
    >
      <Menu.Item key="main" style={{float: 'left'}}>
        <Link to="/">
          <Avatar
          size="small"
          src="https://zenfuse.io/img/zenfuse_logo.svg"
          style={{margin: '10px'}}
          />
          <strong style={{color: 'white'}}>zenfuse</strong>
        </Link>
      </Menu.Item>
      <Menu.Item key="login" style={{float: 'right'}}>
        <Link to="/login">Log in</Link>
      </Menu.Item>
      <Menu.Item key="community" style={{float: 'right'}}>
        <a href="https://t.me/zenfuse_en" target="_blank" rel="noopener noreferrer">Join Community</a>
      </Menu.Item>
      <Menu.Item key="landing" style={{float: 'right'}}>
        <a href="https://zenfuse.io" target="_blank" rel="noopener noreferrer">Go to Landing Page</a>
      </Menu.Item>
    </Menu>
  )


  const renderMenu = () => {
    return getSession()&&getSession().logged ?
    htmlMenuLogged :
    htmlMenuNotLogged
  }

  return renderMenu();
}

export default MenuMain;
