import React, {useEffect} from 'react';
import { Collapse } from 'antd';
import useGlobal from 'store';

const CollapseForAll = (props) => {
  const [globalState, globalActions] = useGlobal();
  const { showExchanges } = globalState;



  return (
  <Collapse
  key={props.key}
  bordered={props.bordered}
  header={props.header}
  onChange={props.onChange}
  >
    {props.elements}
  </Collapse>
  );
}

export default CollapseForAll;
