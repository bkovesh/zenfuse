import React, {useEffect} from 'react';
import { Button, Switch, Avatar, Row, Col, Card, Input, Icon, List } from 'antd';
import * as Components from '../../'
import { Helmet } from 'react-helmet';
import useGlobal from "store";

const { Meta } = Card;

const cardStyle = {
  border:'none',
  padding: '20px 0px'
}

const gridStyle = {
  backgroundColor:'#a4d1ff',
  width: 150,
  height: 80,
  marginRight: 20,
  marginBottom: 20,
  cursor: 'pointer',
  textAlign: 'center',
  borderRadius: 5,
  padding: 10,
}

const gridStyleNew = {
  boxShadow: '0px 0px 0px 1px #a4d1ff',
  width: 150,
  marginRight: 20,
  marginBottom: 20,
  cursor: 'pointer',
  textAlign: 'center',
  borderRadius: 5,
}


function PageSettings() {
  const [globalState, globalActions] = useGlobal();
  const {fetchExchangesInfo,fetchMyExchanges,setVar,getFromStorage} = globalActions.actions;
  const {balances, exchanges, myExchanges, exchangesInfo, trades, modalAddExchange} = globalState;

  useEffect(() => {
    async function init() {
      await fetchExchangesInfo();
      await fetchMyExchanges();
    }
    init();
    renderMyExchanges();
    setInterval(() => {
      renderMyExchanges();
    },1000)
  },[]);

  useEffect(() => {
    renderMyExchanges();
  },[myExchanges]);


  const renderMyExchanges = () => {
    return myExchanges&&myExchanges.map((item,i)=>{
      if (exchangesInfo[item])
      return (
        <Card.Grid key={`${item}-${i}`} style={gridStyle}>
          <img src={exchangesInfo[item].urls.logo} style={{width:'80%'}}/>
          <b style={{fontSize:16}}>{exchangesInfo[item].name}</b>
        </Card.Grid>
      )
    })
  }

  return (
  <div>
    <Helmet title="zenfuse 💎 settings" />
    <div className="row" style={{ margin:'10px 0px' }}>
      <h3>Settings</h3>
    </div>
    <div className="row">
      <div className="col-sm-12 col-md-12">


        {/*User info*/}
        <Card
        title="Change email"
        className="card"
        style={{marginBottom:'20px'}}
        >
          <Row>
            <Col md={{span:12,offset:6}} order={1} style={{textAlign:'center'}}>
              {getFromStorage('session').email}
              <Input size="large" placeholder="New email" style={{margin:10,height:54}} />
              <Input.Password size="large" placeholder="Password" style={{margin:10,height:54}}/>
              <Button
              type="primary" size="large" ghost
              style={{margin:10,height:54,width:150}}
              >
                Save
              </Button>
            </Col>
          </Row>
        </Card>


        {/*User info*/}
        <Card
        title="Change password"
        className="card"
        style={{marginBottom:'20px'}}
        >
          <Row>
            <Col md={{span:12,offset:6}} order={1} style={{textAlign:'center'}}>
              <Input.Password size="large" placeholder="New password" style={{margin:10,height:54}}/>
              <Input.Password size="large" placeholder="Repeat new password" style={{margin:10,height:54}}/>
              <Button
              type="primary" size="large" ghost
              style={{margin:10,height:54,width:150}}
              >
                Save
              </Button>
            </Col>
          </Row>
        </Card>


        {/*Security*/}
        <Card
        title="Security Settings"
        className="card"
        style={{marginBottom:'20px'}}
        >
          <Row style={{color:'red'}}>
            <Col md={{span:24}} order={1}>
              Please, enable 2-factor authentication!
            </Col>
          </Row>
          <Row>
            <Col md={{span:18}} order={1}>
              <h5>2FA authentication</h5>
            </Col>
            <Col md={{span:6}} order={2}>
              <Switch onChange={()=>{}} style={{}}/>
            </Col>
          </Row>
          <Row>
            <Col md={{span:24}} order={1}>
              If enabled, enter Google verification code before each login .
            </Col>
          </Row>
        </Card>


        {/*Exchanges*/}
        <Card
        title="Connected Exchanges"
        className="card"
        style={{marginBottom:'20px'}}
        >
          <Row>
            <Col md={{span:24}} order={1}>
              Edit your API keys and conenct new cryptocurrency exchanges here.
            </Col>
          </Row>
          <Row>
            <Col md={{span:24}} order={1}>
              <Card
              style={cardStyle}
              >
                <Card.Grid
                style={gridStyle}
                onClick={()=>setVar('modalAddExchange',true)}
                >
                  <Icon type="plus" style={{ fontSize: 20, margin:20 }}/>
                </Card.Grid>
                {renderMyExchanges()}
              </Card>
            </Col>
          </Row>
        </Card>



        {/*Delete user*/}
        <Card
        title="Delete account"
        className="card"
        style={{marginBottom:'20px'}}
        >
          <Row>
            <Col md={{span:12,offset:6}} order={1} style={{textAlign:'center'}}>
              <Input.Password size="large" placeholder="Password" style={{margin:10,height:54}}/>
              <Button
              type="danger" size="large" ghost
              style={{margin:10,height:54,width:150}}
              >
                Delete
              </Button>
            </Col>
          </Row>
        </Card>


        <Components.ModalAddExchange
        visible={modalAddExchange}
        handleOk={async ()=>{
          await fetchMyExchanges();
          setVar('modalAddExchange',false)
        }}
        handleCancel={()=>setVar('modalAddExchange',false)}
        />

      </div>
    </div>
  </div>
  )
}

export default PageSettings;
