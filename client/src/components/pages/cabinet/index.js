import React, {useState, useEffect, useReducer} from 'react';
import { Form, Radio, Icon, Input, Button, Checkbox, InputNumber, Slider, Layout, Row, Col, Card, Switch, Pagination, Collapse, Select } from 'antd';
import { Helmet } from 'react-helmet';
import C3Chart from 'react-c3js';
import * as Components from '../../index';
import {prettyTime, prettyTimeShort, prettyNum, prettyNumAuto, prettyTimeDifference} from 'assets/helpers/pretty';
import useGlobal from "store";
const {Panel} = Collapse;
const { Option } = Select;

const donut = {
  data: {
    type: 'donut',
    columns: [
      ['BTC', 30],
      ['ETH', 120]
    ],
  },
  color: {
    pattern: ['#7d62fe', '#46be8a'],
  },
  donut: {
    width: 12,
    label: {
      show: false
    },
  },
}


function PageMain() {
  const [globalState, globalActions] = useGlobal();
  const [modalAddExchange, setModalAddExchange] = useState(false);
  const {setVar,fetchBalances,fetchMyExchanges,fetchTradesByPair,fetchTrades,onRebalanceSliderChange,setInStorage, getFromStorage} = globalActions.actions;
  const {balances, myExchanges, exchanges,exchangesInfo, trades, actTrading,currenciesInfo} = globalState;

  useEffect(() => {
    async function init() {
      if (getFromStorage('session').logged) {
        fetchBalances();
        fetchMyExchanges();
      }
    }
    init()
  },[]);

  useEffect(() => {
    renderBalances()
  },[balances]);

  useEffect(() => {
    renderTrades()
  },[trades]);

  useEffect(() => {
    renderBalances()
  },[actTrading]);

  const refreshAll = async () => {
    fetchBalances();
    fetchTrades();
  }

  let freeBalances = {};
  let usedBalances = {};
  if (balances&&balances.free&&balances.used) {
    freeBalances = balances.free;
    usedBalances = balances.used;
  }

  const renderAction = ()=>{
    if (actTrading==='Balances') {
      return '';
    } else if (actTrading==='Trading') {
      return 'Trading';
    } else if (actTrading==='Rebalance') {
      return 'Rebalance, %';
    }
  }


  ////////////////////////////////
  ////////////////////////////////
  ////////////////////////////////


  const renderTrades = () => {
    console.log('renderTrades',trades)
    let defaultActiveKeys = trades && Object.keys(trades).map((item,i)=>`${item}-${i}`);
    // console.log(defaultActiveKeys)
    return (
    <Collapse
    defaultActiveKey={defaultActiveKeys}
    bordered={false}
    style={{
      borderRadius: 0,
      border: 'none'
    }}
    >
      {trades&&Object.keys(trades).map((item,i) => {
        return (
        <Panel
        key={`${item}-${i}`}
        bordered={false}
        showArrow={false}
        style={{
          padding: 0,
          border: 'none',
        }}
        header={
          <Row
          style={{
            display:'flex',
            flexWrap:'wrap',
            margin:'0px',
            paddingLeft:'25px',
            paddingRight:'25px',
            height:'70px',
            background:'linear-gradient(270deg, #6A11CB 0%, #2575FC 100%)',
            fontSize:'16px',
            fontWeight:'bold',
            color:'white',
            borderBottom:'1px solid #F0F2F6',
          }}
          >
            <Col span={10} sm={{span:10}} style={{margin: 'auto', textAlign:'left', textTransform:'uppercase'}}>
              {item}
            </Col>
            <Col span={6} sm={{span:6}} style={{margin: 'auto', textAlign:'center' }}>

            </Col>
            <Col span={8} sm={{span:8}} style={{ margin: 'auto', paddingLeft: '0', textAlign:'left'}}>

            </Col>
          </Row>
        }
        >
          <Collapse
          bordered={false}
          style={{
            borderRadius: 0,
            border: 'none',
            padding: 0,
          }}
          >
            {Object.keys(trades[item]).map((item2,i2) => {
              if (trades[item][item2].length===0) return;
              return (
              <Panel
              key={`${item2}-${i2}`}
              bordered={false}
              showArrow={false}
              style={{
                padding: 0,
                border: 'none',
              }}
              header={
                <Row
                style={{
                  display:'flex',
                  flexWrap:'wrap',
                  margin:'0px',
                  paddingLeft:'25px',
                  paddingRight:'25px',
                  height:'70px',
                  background: 'rgba(0, 0, 0, 0.015)',
                  fontSize:'16px',
                  fontWeight:'bold',
                  color: 'rgba(0, 0, 0, 0.65)',
                  borderBottom:'1px solid #F0F2F6',
                }}
                >
                  <Col span={14} sm={{span:14}} style={{margin: 'auto', textAlign:'left', textTransform:'uppercase'}}>
                    {currenciesInfo[item2.split('/')[0]]?
                    <img
                    src={`https://www.cryptocompare.com${currenciesInfo[item2.split('/')[0]].ImageUrl}`}
                    style={{padding:'0 5px',width:30}}
                    />:
                    <Icon type="smile" style={{padding:'0 5px',width:40}}/>}
                    {item2}
                    {currenciesInfo[item2.split('/')[1]]?
                    <img
                    src={`https://www.cryptocompare.com${currenciesInfo[item2.split('/')[1]].ImageUrl}`}
                    style={{padding:'0 5px',width:30}}
                    />:
                    <Icon type="smile" style={{padding:'0 5px',width:40}}/>
                    }
                  </Col>
                  <Col span={2} sm={{span:2}} style={{margin: 'auto', textAlign:'center' }}>
                    {/**/}
                  </Col>
                  <Col span={8} sm={{span:8}} style={{ margin: 'auto', paddingLeft: '0', textAlign:'right', fontWeight:'normal'}}>
                    {trades[item][item2].length}
                  </Col>
                </Row>
              }
              >
                <Collapse
                bordered={false}
                style={{
                  borderRadius: 0,
                  border: 'none',
                  padding: 0,
                }}
                >
                  {trades[item][item2].map((item3,i3) => {
                    let {side, symbol, price, amount, cost, timestamp} = item3;
                    let symbol0 = symbol.split('/')[0];
                    let symbol1 = symbol.split('/')[1];
                    return (
                    <Panel
                    key={`${item2}-${i2}-${item3.id}-${i3}`}
                    bordered={false}
                    showArrow={false}
                    style={{
                      padding: 0,
                      border: 'none',
                    }}
                    header={
                      <Row
                      style={{
                        display:'flex',
                        flexWrap:'wrap',
                        margin:'0px',
                        paddingLeft:'25px',
                        paddingRight:'25px',
                        height:'70px',
                        background:'white',
                        fontSize:'14px',
                        color: 'rgba(0, 0, 0, 0.65)',
                        borderBottom:'1px solid #F0F2F6',
                      }}
                      >
                        <Col
                        span={3} sm={{span:3}}
                        style={{
                          margin: 'auto',
                          textAlign:'center',
                          textTransform:'uppercase',
                          color: side==='sell'?'#F45843':'#46be8a'
                        }}
                        >
                          {side}
                          {currenciesInfo[symbol0]?
                          <img
                          src={`https://www.cryptocompare.com${currenciesInfo[symbol0].ImageUrl}`}
                          style={{padding:'0 5px',width:30}}
                          />:
                          <Icon type="smile" style={{padding:'0 5px',width:40}}/>}
                        </Col>
                        <Col span={11} sm={{span:11}} style={{margin: 'auto', textAlign:'center' }}>
                          <div>{prettyTimeShort(timestamp)}</div>
                          <b>{prettyNumAuto(amount)}{' '}{symbol0}</b>
                        </Col>
                        <Col
                        span={10} sm={{span:10}}
                        style={{ margin: 'auto', paddingLeft: '0', textAlign:'right'}}
                        >
                          <div>price: {prettyNumAuto(price)}</div>
                          <div>{prettyNumAuto(cost)}{' '}{symbol1}</div>
                        </Col>
                      </Row>
                    }
                    >
                      <Panel
                      key={`${item2}-${i2}-${item3.id}-${i3}`}
                      bordered={false}
                      showArrow={false}
                      style={{
                        padding: 0,
                        border: 'none',
                      }}
                      header={
                        <Row
                        style={{
                          display:'flex',
                          flexWrap:'wrap',
                          margin:'0px',
                          paddingLeft:'25px',
                          paddingRight:'25px',
                          height:'70px',
                          background:'white',
                          fontSize:'14px',
                          color: 'rgba(0, 0, 0, 0.65)',
                          borderBottom:'1px solid #F0F2F6',
                        }}
                        >
                          <Col span={6} sm={{span:6}} style={{margin: 'auto', textAlign:'left', textTransform:'uppercase'}}>
                            {/*{item3.side}*/}
                          </Col>
                          <Col span={10} sm={{span:10}} style={{margin: 'auto', textAlign:'center' }}>
                            {/*{prettyNumAuto(item3.price)}*/}
                            Full info
                          </Col>
                          <Col span={8} sm={{span:8}} style={{ margin: 'auto', paddingLeft: '0', textAlign:'left'}}>
                            {/*{prettyNumAuto(item3.amount)}*/}
                          </Col>
                        </Row>
                      }
                      >
                        {item3.length}
                      </Panel>
                    </Panel>
                    )
                  })}
                </Collapse>
              </Panel>
              )
            })}
          </Collapse>
        </Panel>
        )
      })}
    </Collapse>
    );
  }


  ////////////////////////////////
  ////////////////////////////////
  ////////////////////////////////



  const renderBalances = () => {
    console.log('renderBalances',balances)
    let res = []
    let exchanges = Object.keys(balances);
    let defaultActiveKeys = exchanges && exchanges.map((item,i)=>`${item}-${i}`);
    exchanges && exchanges.map((item,i)=>{
      let array = [];
      balances[item].info.balances.map((item2,i)=>{
        if (item2.free<=0) return;
        array.push(
        <Panel
        key={`${item2}-${i}`}
        showArrow={false}
        bordered={false}
        disabled={actTrading==='Rebalance'?true:false}
        style={{
          borderRadius: 0,
          border: 'none',
          margin: 0,
        }}
        header={
          <Row
          style={{
            color: 'rgba(0, 0, 0, 0.65)',
            backgroundColor: 'white',
            display:'flex',
            flexWrap:'wrap',
            margin:'0px',
            paddingLeft:'25px',
            paddingRight:'25px',
            fontSize:'16px',
            height:'70px',
            borderBottom:'1px solid #F0F2F6',
          }}
          >
            <Col sm={{span:8,order:1}} style={{ margin:'auto 0', textAlign:'left', fontWeight: 'bold' }}>
              {currenciesInfo[item2.asset]?
              <img src={`https://www.cryptocompare.com${currenciesInfo[item2.asset].ImageUrl}`} style={{padding:'0 5px',width:40}}/>:
              <Icon type="smile" style={{padding:'0 5px',width:40}}/>
              }
              {item2.asset}
            </Col>

            <Col
            sm={{span:12,order:2}}
            style={{display: actTrading==='Rebalance' ? 'block':'none', margin:'auto'}}
            >
              <Row
              style={{
                display:'flex',
                flexWrap:'wrap',
              }}>
                <Col
                sm={{span:12,order:1}}
                style={{margin:'auto 0', paddingRight: 10}}
                >
                  <Slider
                  style={{ marginRight: 20, width: 150 }}
                  min={0}
                  max={100}
                  value={
                    globalState.rebalance[item] ?
                    globalState.rebalance[item][item2.asset] : 0
                  }
                  onChange={(value)=>{
                    onRebalanceSliderChange(item,item2.asset,Number(value))
                  }}
                  />
                </Col>
                <Col
                sm={{span:12,order:2}}
                style={{margin:'auto 0', paddingLeft: 10}}
                >
                  <InputNumber
                  size="small"
                  min={0}
                  max={100}
                  style={{ marginLeft: 10, width: 65 }}
                  value={
                    globalState.rebalance[item] ?
                    globalState.rebalance[item][item2.asset] : 0
                  }
                  onChange={(value)=>{
                    onRebalanceSliderChange(item,item2.asset,Number(value))
                  }}
                  />
                </Col>
              </Row>
            </Col>

            <Col
            sm={{span:12,order:3}}
            style={{display: actTrading==='Rebalance' ? 'none':'block', margin:'auto 0'}}
            >
            </Col>

            <Col
            sm={{span:4,order:4}}
            style={{textAlign:'right', margin:'auto 0', fontSize:'18px'}}>
              {prettyNumAuto(item2.free)}
            </Col>
          </Row>
        }
        >
          <div style={{display:'flex', borderBottom:'1px solid #F0F2F6'}}>
            <div style={{margin:'auto'}}>
              Trading
            </div>
          </div>
        </Panel>
        )
        return;
      });

      res.push(
      <Panel
      key={`${item}-${i}`}
      header={
        <Row
        style={{
          display:'flex',
          flexWrap:'wrap',
          margin:'0px',
          paddingLeft:'25px',
          paddingRight:'25px',
          height:'70px',
          background:'linear-gradient(270deg, #6A11CB 0%, #2575FC 100%)',
          fontSize:'16px',
          fontWeight:'bold',
          color:'white'
        }}
        >
          <Col span={6} sm={{span:6}} style={{margin: 'auto', textAlign:'left', textTransform:'uppercase'}}>
            Binance
          </Col>
          <Col span={10} sm={{span:10}} style={{margin: 'auto', textAlign:'center' }}>
            { renderAction() }
          </Col>
          <Col span={8} sm={{span:8}} style={{ margin: 'auto', paddingLeft: '0', textAlign:'left'}}>
            <div style={{textAlign:'right', fontSize: '18px'}}>
              ${' '}{prettyNumAuto(freeBalances.USDT)}
            </div>
            <div style={{textAlign:'right', fontSize: '14px', fontWeight: 'normal', opacity: 0.8}}>
              {prettyNumAuto(freeBalances.BTC)}{' '}BTC
            </div>
          </Col>
        </Row>
      }
      showArrow={false}
      style={{
        padding: 0,
      }}
      >
        <Collapse
        defaultActiveKey="1"
        style={{
          borderRadius: 0,
          border: 'none',
          padding: 0,
        }}
        >
          {array}
        </Collapse>
      </Panel>
      )
      return;
    })
    return (
    <Collapse
    defaultActiveKey={defaultActiveKeys}
    style={{
      borderRadius: 0,
      border: 'none'
    }}
    >
      {res}
    </Collapse>
    );
  }

  ////////////////////////////////
  ////////////////////////////////
  ////////////////////////////////


  return (
  <div>
    <Helmet title="zenfuse 💎 main" />
    <Row>
      <Col lg={{span:16,order:1}} style={{padding:10}}>


        {/*Total Balance*/}
        <Card className="card">
          <Row>
            <Col sm={{span:12}}>
              <Row>
                <Col xs={{span:12}}>
                  <strong className="totalBalanceTitle">Total Balance</strong>
                  <div className="totalBalance">
                    <small style={{marginRight: '3px'}}>$</small>
                    {prettyNumAuto(freeBalances.USDT)}
                  </div>
                  <div style={{color:'#ACACAC'}}>{prettyNumAuto(freeBalances.BTC)}{' '}BTC</div>
                  <div style={{color:'#ACACAC'}}>{prettyNumAuto(freeBalances.ETH)}{' '}ETH</div>
                </Col>
                <Col xs={{span:12}}>
                  <strong className="totalBalanceTitle">Open Orders</strong>
                  <div className="totalBalance">
                    <small style={{marginRight: '3px'}}>$</small>
                    {prettyNumAuto(usedBalances.USDT)}
                  </div>
                  <div style={{color:'#ACACAC'}}>{prettyNumAuto(usedBalances.BTC)}{' '}BTC</div>
                  <div style={{color:'#ACACAC'}}>{prettyNumAuto(usedBalances.ETH)}{' '}ETH</div>
                </Col>
              </Row>
            </Col>
            <Col sm={{span:12}}>
              <Row>
                <Col xs={{span:12}}>
                  <strong className="totalBalanceTitle">Coin Allocation</strong>
                  <div style={{color:'#ACACAC'}}>10% BTC</div>
                  <div style={{color:'#ACACAC'}}>10% BTC</div>
                  <div style={{color:'#ACACAC'}}>10% ETH</div>
                </Col>
                <Col xs={{span:12}}>
                  <C3Chart
                  style={{width:'110px',height:'110px'}}
                  data={donut.data}
                  color={donut.color}
                  donut={donut.donut}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Card>


        {/*Refresh*/}
        <div style={{ margin:'10px 5px' }}>
          <Row>
            <Col sm={{span:8}} style={{ textAlign:'center', padding:'5px' }}>
              <Button
              icon="sync" className="btnBigOvalShadow"
              style={{ minWidth:'150px' }}
              onClick={refreshAll}
              >
                Refresh
              </Button>
            </Col>
            <Col sm={{span:8}} style={{ textAlign: 'center', padding:'5px' }}>
              <Button
              icon="plus"
              className="btnBigOvalShadow"
              style={{ minWidth:'150px' }}
              onClick={()=>setModalAddExchange(true)}
              >
                Add exchange
              </Button>
            </Col>
            <Col sm={{span:8}} style={{ textAlign: 'center', padding:'5px' }}>
              <Button
              icon="warning"
              className="btnBigOvalShadow"
              style={{ minWidth:'150px', color:'red' }}
              onClick={()=>{}}
              >
                Panic sell
              </Button>
            </Col>
          </Row>
        </div>


        {/*Portfolio*/}
        <Card
        className="card"
        style={{ margin:'0px', marginBottom:'30px', padding:0 }}
        bodyStyle={{padding:0}}
        title={
          <Row>
            <Col lg={{span:12,order:1}}>
              <strong style={{ marginRight:'25px' }}>Portfolio</strong>
              <Radio.Group
              defaultValue="Exchanges"
              style={{ marginRight:'25px'}}
              onChange={(e)=>setVar('actPortfolio',e.target.value)}
              >
                <Radio.Button value="Exchanges">Exchanges</Radio.Button>
                <Radio.Button value="Coins">Coins</Radio.Button>
              </Radio.Group>
            </Col>

            <Col lg={{span:12,order:2}} >
              <Radio.Group
              defaultValue="Balances"
              style={{ marginRight:'25px'}}
              onChange={(e)=>setVar('actTrading',e.target.value)}
              >
                <Radio.Button value="Balances">Balances</Radio.Button>
                <Radio.Button value="Trading">Trading</Radio.Button>
                <Radio.Button value="Rebalance">Rebalance</Radio.Button>
              </Radio.Group>
            </Col>

          </Row>
        }
        >


          {balances&&Object.keys(balances).length>0 ? (
          <Row>
            <Col>
              {renderBalances()}
            </Col>
          </Row>
          ) : (
          <Row style={{ height: '70px', borderBottom: '1px solid #F0F2F6' }}>
            <Col style={{ margin: '25px auto', textAlign:'center', color:'#c9c9c9' }}>
              nothing yet
            </Col>
          </Row>
          )}

        </Card>
      </Col>






      {/*Orders*/}
      <Col lg={{span:8,order:2}} style={{padding:10}}>
        <Card
        className="card"
        bodyStyle={{ padding:0 }}
        title={
          <div>
            <strong style={{ marginRight:'25px' }}>Orders</strong>
            <Radio.Group
            defaultValue="Active"
            onChange={(e)=>setVar('actOrders',e.target.value)}
            >
              <Radio.Button value="Recent">Recent</Radio.Button>
              <Radio.Button value="Active">Active</Radio.Button>
            </Radio.Group>
          </div>
        }
        >
          <Row>
            <Col>
              <Select
              showSearch
              style={{ width: '90%', margin:10 }}
              // size="large"
              placeholder="Pair"
              optionFilterProp="children"
              onChange={() => console.log('sss')}
              filterOption={(input, option) =>
              option.props.children[1].toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              >
                {exchanges && exchanges.map((item,i) => {
                  return(
                  <Option key={`option-${item}-${i}`} value={item}>
                    {exchangesInfo&&exchangesInfo[item]&&<img src={exchangesInfo[item].urls.logo} style={{padding:'0 5px'}}/>}
                    {item}
                  </Option>
                  )
                })}
              </Select>
            </Col>
          </Row>
          <Row>
            <Col>
              {renderTrades()}
            </Col>
          </Row>

          <Row style={{ height: '70px', padding: 15 }}>
            <Col>
              <Pagination size="small" total={50} showSizeChanger />
            </Col>
          </Row>
        </Card>
      </Col>


    </Row>


    <Components.ModalAddExchange
    visible={modalAddExchange}
    handleOk={()=>{setModalAddExchange(false)}}
    handleCancel={()=>{setModalAddExchange(false)}}
    />
  </div>
  );
}


export default PageMain;
