import React from 'react'
import { Button, Form, Input, Checkbox, Radio, Icon, message } from 'antd'
import { useHistory, Link } from "react-router-dom";

import { Helmet } from 'react-helmet'
import styles from './style.module.scss'
import useGlobal from "store";

function PageLogin(props) {
  let history = useHistory();
  const [globalState, globalActions] = useGlobal();
  const {setVar, reg} = globalActions.actions;
  const {actLogin,email,password,passwordRep} = globalState;

  const onRegister = async () => {
    let result = await reg({email,password,passwordRep})
    if (result&&result.success===false&&result.message)  message.warning(result.message,4);
    if (result&&result.success===true) message.success('You are registered. Please, confirm your email.',4);
    history.push("/cabinet");
    return;
  }

  return (
  <div>
    <Helmet title="zenfuse 💎 login" />
    <div
    className={styles.content}
    style={{ color: 'grey', minWidth:300, maxWidth:500 }}
    >
      <div className={styles.promo}>
        <Radio.Group
        checked={actLogin}
        defaultChecked={actLogin}
        size="large"
        >
          <Radio.Button
          value="Login"
          onClick={(e)=>{
            setVar('actLogin',e.target.value)
            history.push("/login")
          }}
          >
            Log in
          </Radio.Button>
          <Radio.Button
          value="Register"
          onClick={(e)=>{
            setVar('actLogin',e.target.value)
            history.push("/register")
          }}
          >
            Register
          </Radio.Button>
        </Radio.Group>
      </div>
      <div className={styles.form}>
        <Form className="login-form">
          <Form.Item>
            <Input
            placeholder="Email"
            style={{height: '54px'}}
            onChange={(e)=>setVar('email',e.target.value)}
            />
          </Form.Item>
          <Form.Item>
            <Input.Password
            placeholder="Password"
            style={{height: '54px'}}
            onChange={(e)=>setVar('password',e.target.value)}
            />
          </Form.Item>
          <Form.Item>
            <Input.Password
            placeholder="Repeat Password"
            style={{height: '54px'}}
            onChange={(e)=>setVar('passwordRep',e.target.value)}
            />
          </Form.Item>
          <Form.Item>
            <Checkbox checked={true}>Agree with Terms and Conditions</Checkbox>
            <Link to="/terms">(Read)</Link>
          </Form.Item>
          <Form.Item>
            <Button
            type="primary" block htmlType="submit"
            style={{height: '54px'}}
            onClick={onRegister}
            >
              Register
            </Button>
          </Form.Item>
          <div className="form-group">
            <Button block style={{height: '54px'}} htmlType="submit" className="login-form-button">
              Log In with <Icon type="google" />
            </Button>
          </div>
        </Form>
      </div>
    </div>
  </div>
  )
}

export default PageLogin;