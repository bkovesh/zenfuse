import React from 'react'
import { Button, Form, Input, Checkbox, Radio, Icon, message } from 'antd'
import { BrowserRouter, Route, Switch, Redirect, Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import styles from './style.module.scss'
import { useHistory } from "react-router-dom";
import useGlobal from "store";

function PageLogin(props) {
  let history = useHistory();
  const [globalState, globalActions] = useGlobal();
  const {setVar,login} = globalActions.actions;
  const {actLogin, email, password} = globalState;

  const onLogin = async () => {
    let result = await login({email,password})
    // console.log('onLogin',result)
    if (result&&result.success===false&&result.message) return message.warning(result.message,4);
    history.push("/cabinet");
  }


  return (
    <div>
      <Helmet title="zenfuse 💎 login" />
      {/*<section*/}
        {/*className={styles.login}*/}
      {/*>*/}
        <div
        className={styles.content}
        style={{ color: 'grey', minWidth:300, maxWidth:500 }}
        >
          <div className={styles.promo}>
            <Radio.Group
            checked={actLogin}
            defaultChecked={actLogin}
            size="large"
            >
              <Radio.Button
              value="Login"
              onClick={(e)=>{
                setVar('actLogin',e.target.value)
                history.push("/login")
              }}
              >
                Log in
              </Radio.Button>
              <Radio.Button
              value="Register"
              onClick={(e)=>{
                setVar('actLogin',e.target.value)
                history.push("/register")
              }}
              >
                Register
              </Radio.Button>
            </Radio.Group>
          </div>
          <div className={styles.form}>
            <Form className="login-form">
              <Form.Item>
                <Input
                placeholder="Email"
                style={{height: '54px'}}
                onChange={(e)=>setVar('email',e.target.value)}
                />
              </Form.Item>
              <Form.Item>
                <Input.Password
                placeholder="Password"
                style={{height: '54px'}}
                onChange={(e)=>setVar('password',e.target.value)}
                />
              </Form.Item>
              <Form.Item>
                <Checkbox>Remember me</Checkbox>
                <Link to="/remind">Forgot password?</Link>
              </Form.Item>
              <Form.Item>
                <Button
                type="primary" block htmlType="submit"
                style={{height: '54px'}}
                onClick={onLogin}
                >
                  Log in
                </Button>
              </Form.Item>
              <div className="form-group">
                <Button block style={{height:'54px'}} htmlType="submit" className="login-form-button">
                  Log in with <Icon type="google" />
                </Button>
              </div>
            </Form>
          </div>
        </div>
      {/*</section>*/}
    </div>
  )
}

export default PageLogin
