import React, {useEffect, useMemo} from 'react';
import { Form, Radio, Icon, Input, Button, Checkbox, InputNumber, Slider, Layout, Row, Col, Card, Switch, Pagination, Collapse, Avatar, Select } from 'antd';
import useGlobal from "store";

///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
const { Panel } = Collapse;
const { Option } = Select;


const customPanelStyle = {
  // background: '#f7f7f7',
  borderRadius: 0,
  // marginBottom: 24,
  // border: 'none',
  overflow: 'hidden',
};


const customPanelStyleBottom = {
  // background: '#f7f7f7',
  borderRadius: 0,
  // marginBottom: 24,
  border: 'none',
  overflow: 'hidden',
};

const customPanelStyleTop = {
  // background: '#f7f7f7',
  borderRadius: 0,
  // marginBottom: 24,
  borderTop: '1px solid #d9d9d9',
  overflow: 'hidden',
};

const panelHeaderStyle = {
  padding: 5,
  fontWeight: 'bold',
  fontSize: 18,
  textTransform: 'uppercase',
}

const panelHeaderStyle2 = {
  fontWeight: 'bold',
  fontSize: 14,
  textTransform: 'uppercase',
}

///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////

function PageMarkets() {
  const [globalState, globalActions] = useGlobal();
  const {fetchExchangesInfo, fetchCurrenciesInfo,fetchMarkets, fetchBalances, fetchTrades, fetchTicker, setVar, sleep} = globalActions.actions;
  const {balances, exchanges, currenciesInfo, exchangesInfo, trades, actTrading, showExchanges, htmlSymbols, htmlExchanges, symbols} = globalState;


  useMemo(()=> {
    // executes before rendering
    fetchExchangesInfo()
  },[]);


  useEffect(()=>{
    (async () => {
      await renderExchanges();
      await renderSymbols();
    })();
  },[showExchanges,])


  const renderSymbols = async () => {
    console.log('renderSymbols showExchanges',showExchanges);
    for (let item of showExchanges) {
      if (Object.keys(htmlSymbols).indexOf(item)>-1) continue;
      let markets = await fetchMarkets(item);
      symbols[item] = Object.keys(markets);
      setVar('symbols',symbols);
      let arr = [];
      for (let key in markets) {
        arr.push(
          <Panel
          header={
            <div style={panelHeaderStyle2}>
              <div style={{textAlign:'left', width:50, display:'inline-block'}}>
                {exchangesInfo&&exchangesInfo[item]&&currenciesInfo[key.split('/')[0]]?
                <img
                src={`https://www.cryptocompare.com${currenciesInfo[key.split('/')[0]].ImageUrl}`}
                style={{padding:'0 5px',width:30}}
                />:
                <Icon type="smile" style={{padding:'0 5px',width:30}}/>
                }
              </div>

              <div style={{textAlign:'center', width:150, display:'inline-block'}}>
                {key}
              </div>

              <div style={{textAlign:'right', width:50, display:'inline-block'}}>
                {exchangesInfo&&exchangesInfo[item]&&currenciesInfo[key.split('/')[1]]?
                <img
                src={`https://www.cryptocompare.com${currenciesInfo[key.split('/')[1]].ImageUrl}`}
                style={{padding:'0 5px',width:30}}
                />:
                <Icon type="smile" style={{padding:'0 5px',width:30}}/>
                }
              </div>
            </div>
          }
          key={`${item}-${key}`}
          showArrow={false}
          >
            <p>some things would be here</p>
          </Panel>
        )
      }
      htmlSymbols[item] = arr;
      setVar('htmlSymbols',htmlSymbols);
    }
  }


  const renderExchanges = () => {
    // console.log('renderExchanges exchangesInfo',exchangesInfo)
    console.log('renderExchanges currenciesInfo',currenciesInfo)
    return (
    <Collapse
    bordered={false}
    onChange={(e)=>{
      setVar('showExchanges',e);
    }}
    >
      {exchanges && exchanges.map((item,i)=>{
        return (
        <Panel
        style={customPanelStyle}
        header={
          <div style={panelHeaderStyle}>
            {exchangesInfo&&exchangesInfo[item]&&<img src={exchangesInfo[item].urls.logo} style={{padding:'0 5px'}}/>}
            {item}
          </div>
        }
        key={item}
        showArrow={false}
        >
          <Collapse
          bordered={false}
          onChange={(e)=>{
            // setVar('showExchanges',e);
          }}
          >
            {htmlSymbols[item]}
          </Collapse>
        </Panel>
        )
      })}
    </Collapse>
    )
  }


  return (
    <div>
      <div>
        <h2>Markets</h2>
        <span style={{ width: 200, marginRight:10 }}>Search pairs:</span>

        <Select
        showSearch
        style={{ width: 200, margin:10 }}
        size="large"
        placeholder="Exchange"
        optionFilterProp="children"
        onChange={() => console.log('sss')}
        filterOption={(input, option) =>
          option.props.children[1].toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        >
          {exchanges && exchanges.map((item,i) => {
            return(
            <Option key={`option-${item}-${i}`} value={item}>
              {exchangesInfo&&exchangesInfo[item]&&<img src={exchangesInfo[item].urls.logo} style={{padding:'0 5px'}}/>}
              {item}
            </Option>
            )
          })}
        </Select>

        <Select
        showSearch
        style={{ width: 200, margin:'10px 0px' }}
        size="large"
        placeholder="Pair"
        optionFilterProp="children"
        // onChange={onChange}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        >
          <Option value="jack">Jack</Option>
          <Option value="lucy">Lucy</Option>
          <Option value="tom">Tom</Option>
        </Select>

      </div>
      <Card className="card" bodyStyle={{padding:0}}>
        {renderExchanges()}
      </Card>
    </div>
  );
}


export default PageMarkets;
