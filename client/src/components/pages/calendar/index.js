import React from 'react';
import { Button, Switch, Calendar } from 'antd';
import { Helmet } from 'react-helmet'

function PageCalendar() {

  return (
  <div>
    <Helmet title="zenfuse 💎 calendar" />
    <div className="row" style={{ margin:'10px 0px' }}>
      <h3>Calendar of orders</h3>
    </div>
    <div className="row">
      <div className="col-sm-12 col-md-12">
        <Calendar/>
      </div>
    </div>
  </div>
  )
}

export default PageCalendar;
