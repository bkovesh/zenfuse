import React from 'react'
import { Button, Form, Input, Checkbox, Radio, Icon } from 'antd'
import { BrowserRouter, Route, Switch, Redirect, Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import styles from './style.module.scss'
import { useHistory } from "react-router-dom";
import useGlobal from "store";

function Remind(props) {
  let history = useHistory();
  const [globalState, globalActions] = useGlobal();
  const {setVar} = globalActions.actions;
  const {actLogin} = globalState;

  return (
    <div>
      <Helmet title="zenfuse 💎 login" />
      <div
      className={styles.content}
      style={{ color: 'grey', minWidth:300, maxWidth:500 }}
      >
        <div className={styles.promo}>
          <Radio.Group
          checked={actLogin}
          defaultChecked={actLogin}
          size="large"
          >
            <Radio.Button
            value="Login"
            onClick={(e)=>{
              setVar('actLogin',e.target.value)
              history.push("/login")
            }}
            >
              Log in
            </Radio.Button>
            <Radio.Button
            value="Register"
            onClick={(e)=>{
              setVar('actLogin',e.target.value)
              history.push("/register")
            }}
            >
              Register
            </Radio.Button>
          </Radio.Group>
        </div>
        <div className={styles.form}>
          <Form className="login-form">
            <Form.Item>
              <Input
              placeholder="Email"
              style={{height: '54px'}}
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" block htmlType="submit" style={{height: '54px'}} className="login-form-button">
                Remind
              </Button>
            </Form.Item>
            <div className="form-group">
              <Button block style={{height: '54px'}} htmlType="submit" className="login-form-button">
                Log In with <Icon type="google" />
              </Button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  )
}

export default Remind
