import React from 'react';


function Page404() {

  return (
  <div style={{textAlign: 'center'}}>
    <h2>Empty page</h2>
    <h3>It means, that this page does not exist.</h3>
  </div>
  );
}

export default Page404;
