
export const prettyTime = (timestamp) => {
  const date = new Date(timestamp)
  const options = {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
  }
  return date.toLocaleDateString('en-GB', options)
}

export const prettyTimeShort = (timestamp) => {
  const date = new Date(timestamp)
  const options = {
    month: 'short',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }
  return date.toLocaleDateString('en-GB', options)
}


export const prettyTimeDifference = (current, previous) => {
  var msPerMinute = 60 * 1000;
  var msPerHour = msPerMinute * 60;
  var msPerDay = msPerHour * 24;
  var msPerMonth = msPerDay * 30;
  var msPerYear = msPerDay * 365;
  var elapsed = current - previous;

  if (elapsed < msPerMinute) {
    return Math.round(elapsed/1000) + ' secs ago';
  }
  else if (elapsed < msPerHour) {
    return Math.round(elapsed/msPerMinute) + ' mins ago';
  }
  else if (elapsed < msPerDay ) {
    return Math.round(elapsed/msPerHour ) + ' hrs ago';
  }
  else if (elapsed < msPerMonth) {
    return Math.round(elapsed/msPerDay) + ' days ago';
  }
  else {
    return prettyTime(previous);
  }
}


export const prettyNum = (value, num) => {
  value = value ? value : 0
  return Number(value).toFixed(num)
}


export const prettyNumAuto = (value) => {
  value = value ? value : 0
  if (value===0) {
    value = Number(value).toFixed(0)
  }
  else if (value>0.1&&value<0) {
    value = Number(value).toFixed(2)
  }
  else if (value>0.01&&value<=0.1) {
    value = Number(value).toFixed(3)
  }
  else if (value>0.001&&value<=0.01) {
    value = Number(value).toFixed(4)
  }
  else if (value>0.0001&&value<=0.001) {
    value = Number(value).toFixed(5)
  }
  else if (value>0.00001&&value<=0.0001) {
    value = Number(value).toFixed(6)
  }
  else if (value<=0.00001) {
    value = Number(value).toFixed(7)
  }
  else if (value>1000) {
    value = Number(value).toFixed(1)
  }
  else {
    value = Number(value).toFixed(3)
  }
  return value
}
