import React, {useEffect} from 'react'
import { Form, Icon, Input, Button, Checkbox, Layout, Row, Col, Card, Menu, Avatar } from 'antd';
import { BrowserRouter, Route, Switch, Redirect, Link } from 'react-router-dom'
import Loadable from 'react-loadable'
import * as Components from 'components'
import useGlobal from "store";
import PageLogin from './components/pages/login'


const Loading = () => (
  <div style={{width:'100%', textAlign:'center'}}>
    <Icon type="loading" />
  </div>
);

const loadable = (loader) => Loadable({
  loader,
  delay: false,
  loading: Loading,
})


const login = Loadable({
  loader: () => import(`./components/pages/login`),
  loading: Loading,
});



const routes = [
  {
    path: '/',
    component: loadable(() => import('./components/pages/main')),
    exact: true,
  },
  {
    path: '/login',
    component: loadable(() => import('./components/pages/login')),
    exact: true,
  },
  {
    path: '/register',
    component: loadable(() => import('./components/pages/register')),
    exact: true,
  },
  {
    path: '/remind',
    component: loadable(() => import('./components/pages/remind')),
    exact: true,
  },
]

const routesPrivate = [
  {
    path: '/cabinet',
    component: loadable(() => import('./components/pages/cabinet')),
    exact: true,
  },
  {
    path: '/calendar',
    component: loadable(() => import('./components/pages/calendar')),
    exact: true,
  },
  {
    path: '/settings',
    component: loadable(() => import('./components/pages/settings')),
    exact: true,
  },
]



function Router(props) {
  const [globalState, globalActions] = useGlobal();
  const {fetchCurrenciesInfo, fetchExchanges, fetchMyExchanges, fetchMarkets, fetchBalances, fetchTrades, fetchTicker, getFromStorage,setVar,fetchExchangesInfo} = globalActions.actions;
  const {session} = globalState;
  const { history } = props;

  const getSession = () => {
    return getFromStorage('session');
  }

  useEffect(() => {
    async function run() {
      if (getSession()&&getSession().logged) {
        fetchBalances();
        fetchTrades();
        fetchMyExchanges();
      }
      fetchExchanges();
      fetchExchangesInfo();
      fetchCurrenciesInfo();
    }
    run();
    // setInterval(()=>{
    //   run();
    // },10000)
    return () => {
      // returned function will be called on component unmount
    }
  },[]);


  return (
    <BrowserRouter history={history}>
      <Components.MenuMain/>
      <div className="mainContainer">
        <Switch>
          {/*<Route exact path="/" render={() => <Redirect to="/" />} />*/}
          {
            routesPrivate.map(route => (
              <Route
              path={route.path}
              component={ getSession()&&getSession().logged ? route.component : login}
              key={route.path}
              exact={route.exact}
              />
            ))
          }
          {
            routes.map(route => (
              <Route
              path={route.path}
              component={route.component}
              key={route.path}
              exact={route.exact}
              />
            ))
          }
          <Route
          path="*"
          component={loadable(() => import('./components/pages/404'))}
          key="*"
          exact={true}
          />
        </Switch>
      </div>
    </BrowserRouter>
  )
}


export default Router
