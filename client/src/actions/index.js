import * as ccxt from './ccxt';
import * as rebalance from './rebalance';
import * as storage from './storage';
import * as ui from './ui';
import * as user from './user';


const actions = {
  ...ccxt,
  ...rebalance,
  ...storage,
  ...ui,
  ...user,
};

export {actions};