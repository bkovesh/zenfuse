import ccxt from 'ccxt'
import config from 'config/config'
import * as storage from './storage';



// Get all exchanges names
export const fetchCurrenciesInfo = async (store) => {
  const url = 'https://min-api.cryptocompare.com/data/all/coinlist'
  const promise = await fetch(url,{
    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json'},
    method: "GET",
  })
  const result = await promise.json();
  if (result&&result.Response==='Success') {
    store.setState({ currenciesInfo: result.Data });
  } else {
    store.setState({ currenciesInfo: {} });
  }
}


// Get all exchanges names
export const fetchExchanges = (store) => {
  store.setState({ exchanges: ccxt.exchanges });
}

// Get my exchanges names
export const fetchMyExchanges = async (store) => {
  const session = await storage.getFromStorage(store,'session')
  const body = {
    userId: session.userId,
    sessionId: session.sessionId,
  }
  const promise = await fetch(config.urlServer+'/my-exchanges',{
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "POST",
    body: JSON.stringify(body)
  })
  const result = await promise.json();
  if (result.success) {
    store.setState({ myExchanges:result.data.reverse() });
  } else {
    store.setState({ myExchanges: [] });
  }
}


// Get my exchanges names
export const addExchange = async (store,exchange) => {
  const session = await storage.getFromStorage(store,'session')
  const body = {
    userId: session.userId,
    sessionId: session.sessionId,
    exchange: exchange,
  }
  const promise = await fetch(config.urlServer+'/add-exchange',{
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "POST",
    body: JSON.stringify(body)
  })
  const result = await promise.json();
  // if (result.success) {
  //   store.setState({ myExchanges:result.data });
  // } else {
  //   store.setState({ myExchanges:[] });
  // }
}

// Get all exchanges objects
export const fetchExchangesInfo = async (store) => {
  const promise = await fetch(config.urlServer+'/exchanges-info');
  const exchangesInfo = await promise.json();
  console.log('fetchExchangesInfo',exchangesInfo);
  store.setState({ exchangesInfo });
}

export const fetchBalances = async (store) => {
  const session = await storage.getFromStorage(store,'session')
  // console.log(session)
  const body = {
    userId: session.userId,
    sessionId: session.sessionId,
  }
  const promise = await fetch(config.urlServer+'/all-balances',{
    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    method: "POST",
    body: JSON.stringify(body)
  })
  const result = await promise.json();
  console.log('fetchBalances',result);
  if (result.success) {
    store.setState({ balances: result.data });
  } else {
    store.setState({ balances: {} });
  }
}

export const fetchTrades = async (store) => {
  const session = await storage.getFromStorage(store,'session')
  const body = {
    userId: session.userId,
    sessionId: session.sessionId,
  }
  const promise = await fetch(config.urlServer+'/all-my-orders',{
    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    method: "POST",
    body: JSON.stringify(body)
  })
  const result = await promise.json();
  console.log('fetchTrades',result)
  if (result.success) return store.setState({ trades: result.data });
  return store.setState({ trades: {} });
}

export const fetchTradesByPair = async (store,exchange,pair,since,limit) => {
  const session = await storage.getFromStorage(store,'session')
  const body = {
    userId: session.userId,
    sessionId: session.sessionId,
    exchange: exchange,
    pair,
    since,
    limit,
  }
  const promise = await fetch(config.urlServer+'/my-orders',{
    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    method: "POST",
    body: JSON.stringify(body)
  })
  const result = await promise.json();
  console.log('fetchTradesByPair',result)
  if (result.success) return result.data;
  return {};
}


export const buy = () => {
  console.log('buy')
  // let data = {}
  // fetch(config.urlServer+'/buy', {
  //   method: 'POST',
  //   body: data,
  //   headers: {
  //     'Content-Type': 'application/json'
  //   }
  // });
}


export const sell = () => {
  console.log('sell')
  // let data = {}
  // fetch(config.urlServer+'/sell', {
  //   method: 'POST',
  //   body: data,
  //   headers: {
  //     'Content-Type': 'application/json'
  //   }
  // });
}


export const sellAll = () => {
  console.log('sellAll')
  // let data = {}
  // fetch(config.urlServer+'/sell', {
  //   method: 'POST',
  //   body: data,
  //   headers: {
  //     'Content-Type': 'application/json'
  //   }
  // });
}



export const fetchMarkets = async (store,exchangeName) => {
  const url = config.urlServer+`/markets/${exchangeName}`
  const promise = await fetch(url,{
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "GET",
  })
  const result = await promise.json();
  console.log('fetchMarkets',result)
  if (result.success) {
    if ( typeof result.data === 'string') result.data = {}; // TODO: removing exchangeError
    store.setState({ markets: result.data });
  } else {
    store.setState({ markets: {} });
  }
  return result.data;

  // let opts = {
  //   apiKey: process.env.REACT_APP_BINANCE_API_KEY,
  //   secret: process.env.REACT_APP_BINANCE_SECRET_KEY,
  //   timeout: 30000,
  //   enableRateLimit: true,
  // }
  // let exchange = new ccxt[exchangeName](opts);
  // exchange.proxy = exchange.has.CORS?undefined:'https://cors-anywhere.herokuapp.com/'
  // let markets = await exchange.loadMarkets()
  // store.setState({ markets: markets });
  // console.log('fetchMarkets',markets,exchange);
  // return markets;
}



export const fetchTicker = async (store) => {
  // const proxy = 'https://crossorigin.me/'
  const proxy = 'https://cors-anywhere.herokuapp.com/'
  let opts = {
    // apiKey: process.env.REACT_APP_BINANCE_API_KEY,
    // secret: process.env.REACT_APP_BINANCE_SECRET_KEY,
    // timeout: 30000,
    // enableRateLimit: true,
    proxy
  }
  let exchange = new ccxt['kraken'](opts)
  let markets = await exchange.loadMarkets()
  let symbols = Object.keys(markets)
  let ticker = await exchange.fetchTicker(symbols[0])
  let timeframes = await exchange.timeframes
  let ohlcv = await exchange.fetchOHLCV(symbols[0], '1m')
  let orderBook = await exchange.fetchOrderBook(symbols[0])
  if (exchange.has.fetchOHLCV) {
    // for (let symbol in exchange.markets) {
    //   await sleep (exchange.rateLimit) // milliseconds
    //   console.log (await exchange.fetchOHLCV (symbol, '1m')) // one minute
    // }
  } else {
    console.log('fetchTicker','exchange has no OHLCV data')
  }
}


///////////////////////////////////
///////////////////////////////////
///////////////////////////////////


export const fetchMarketsInBrowser = async (store) => {
  // TODO: could use 'cors-anywhere' package on our own server for CORS-proxying https://github.com/ccxt/ccxt/issues/2052
  // const proxy = 'https://crossorigin.me/'
  const proxy = 'https://cors-anywhere.herokuapp.com/'
  let exchanges = ccxt.exchanges
  let allMarkets = []
  let opts = {
    // apiKey: process.env.REACT_APP_BINANCE_API_KEY,
    // secret: process.env.REACT_APP_BINANCE_SECRET_KEY,
    // timeout: 30000,
    // enableRateLimit: true,
    proxy
  }
  let markets = new ccxt['kraken'](opts).loadMarkets()

  // exchanges.map(async (item,i)=>{
  //   if (i>3) return
  //   let exchange
  //   try {
  //     exchange = new ccxt[item]({
  //       // apiKey: process.env.REACT_APP_BINANCE_API_KEY,
  //       // secret: process.env.REACT_APP_BINANCE_SECRET_KEY,
  //       // timeout: 30000,
  //       // enableRateLimit: true,
  //       proxy
  //     })
  //     let markets = await exchange.loadMarkets()
  //     allMarkets.push(markets)
  //   } catch (e) {
  //     return
  //   }
  // })
  // allMarkets = Promise.all(allMarkets)

  // let balance = await exchange.fetchBalance({'recvWindow': 60000})
  // filter pairs by balance.used
  // let pair = 'BTC/USDT'
  // let since = exchange.milliseconds() - 86400000 * 10;

  // console.log('fetchExchangesInBrowser exchanges', exchanges)
  console.log('fetchExchangesInBrowser markets', await markets)
  // console.log('fetchExchangesInBrowser allMarkets', allMarkets)
  // console.log('fetchExchangesInBrowser balance', balance)
  // console.log('fetchExchangesInBrowser orders', orders)
}





const sleep = (t) => setTimeout(()=>{},t)



