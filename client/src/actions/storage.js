
export const setInStorage = (store, key, obj) => {
  console.log('setInStorage',key,obj)
  if (!key) { console.error('Error: key is missing') }
  try {
    localStorage.setItem(key, JSON.stringify(obj));
  } catch (err) {
    console.error(err);
  }
}

export const getFromStorage = (store, key) => {
  if (!key) { console.error('Error: key is missing'); return null; }
  try {
    const valueString = localStorage.getItem(key);
    if (valueString) return JSON.parse(valueString);
    return null;
  } catch (err) {
    return null;
  }
}

