import config from 'config/config'
import {setInStorage, getFromStorage} from './storage';


export const login = async (store,body) => {
  const response = await fetch(config.urlServer+'/login',{
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "POST",
    body: JSON.stringify(body)
  })
  const result = await response.json();
  console.log('login',result);
  if (result&&result.success===true) {
    setInStorage(store,'session',{
      logged:true,
      email: body.email,
      sessionId:result.sessionId,
      userId:result.userId,
    });
    store.setState({
      logged: true,
      email: body.email,
      sessionId: result.sessionId,
      userId: result.userId
    });
  }
  return result;
}


export const logout = async (store,body) => {
  const response = await fetch(config.urlServer+'/logout',{
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "POST",
    body: JSON.stringify(body)
  })
  // console.log('logout',response)
  const result = await response.json()
  console.log('logout',result)
  if (result&&result.success===true) {
    setInStorage(store,'session',{
      logged:false,
      email: body.email,
      sessionId:result.sessionId,
      userId:result.userId,
    });
    store.setState({
      logged: false,
      email: body.email,
      sessionId: result.sessionId,
      userId: result.userId
    });
  }
  return result;
}


export const reg = async (store,body) => {
  try {
    const response = await fetch(config.urlServer+'/reg',{
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify(body)
    })
    console.log('reg',body);
    const result = await response.json();
    // console.log('reg',result);
    if (result&&result.success===true) {
      setInStorage(store,'session',{
        logged:true,
        email: body.email,
        sessionId:result.sessionId,
        userId:result.userId,
      });
      store.setState({
        logged: true,
        email: body.email,
        sessionId: result.sessionId,
        userId: result.userId
      });
    }
    return result;
  } catch (err) {
    console.error('reg',err)
  }
}
