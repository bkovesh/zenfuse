const express = require("express");
var app = express();
const ccxt = require("ccxt");
var bodyParser = require("body-parser");

// from variable id
// const exchangeId = "binance",
//   exchangeClass = ccxt[exchangeId],
//   exchange = new exchangeClass({
//     apiKey: "s1LTw8uRFVGH1OBxo0oXn10uYL9vEJ1NJ0xY0P2FVfgrJSqQjkiwcbk4PRZTXD8W",
//     secret: "MKEBUP1s5zMOSPVF0Yg1QVBrOyv0SIm3M93q7sjVOSqX38aDfcUDCqf62IC2EWQH",
//     timeout: 30000,
//     enableRateLimit: true
//   });

class Zenfuse {
  getAllExchanges() {
    return ccxt.exchanges;
  }

  async getBinanceMarkets(exchangeName, apiKey, secret) {
    let exchangeId = exchangeName;
    let exchangeClass = ccxt[exchangeId];
    let exchange = new exchangeClass({
      apiKey: apiKey,
      secret: secret,
      timeout: 300000,
      enableRateLimit: true
    });
    // let result = exchange.markets["btcusd"];
    try {
      let result = await exchange.loadMarkets();
      return result;
    } catch (e) {
      return e.message;
    }
  }

  // Получение параметров пары по ID
  async getBinanceMarketById(pairId, exchangeName, apiKey, secret) {
    let exchangeId = exchangeName;
    let exchangeClass = ccxt[exchangeId];
    let exchange = new exchangeClass({
      apiKey: apiKey,
      secret: secret,
      timeout: 30000,
      enableRateLimit: true
    });
    // let result = exchange.markets["btcusd"];
    await exchange.loadMarkets();
    return exchange.markets_by_id[pairId.toString().toUpperCase()];
  }

  async getBinanceBalance(exchangeName, apiKey, secret) {
    let exchangeId = exchangeName;
    let exchangeClass = ccxt[exchangeId];
    let exchange = new exchangeClass({
      apiKey: apiKey,
      secret: secret,
      timeout: 30000,
      enableRateLimit: true
    });
    return await exchange.fetchBalance();
  }

  async buyBinanceByCurrentPrice(pair, amount, exchangeName, apiKey, secret) {
    let exchangeId = exchangeName;
    let exchangeClass = ccxt[exchangeId];
    let exchange = new exchangeClass({
      apiKey: apiKey,
      secret: secret,
      timeout: 30000,
      enableRateLimit: true
    });
    // await console.log(`Buying: ${firstCurrency}/${secondCurrency} - ${amount}`);
    return await exchange.createMarketBuyOrder(pair, amount);
  }

  async sellBinanceByCurrentPrice(pair, amount, exchangeName, apiKey, secret) {
    let exchangeId = exchangeName;
    let exchangeClass = ccxt[exchangeId];
    let exchange = new exchangeClass({
      apiKey: apiKey,
      secret: secret,
      timeout: 30000,
      enableRateLimit: true
    });
    // await console.log(`Buying: ${firstCurrency}/${secondCurrency} - ${amount}`);
    return await exchange.createMarketSellOrder(pair, amount);
  }

  async getAllMyTrades(pair, exchangeName, apiKey, secret) {
    let exchangeId = exchangeName;
    let exchangeClass = ccxt[exchangeId];
    let exchange = new exchangeClass({
      apiKey: apiKey,
      secret: secret,
      timeout: 30000,
      enableRateLimit: true
    });
    let since = exchange.milliseconds() - 86400000 * 4;
    try {
      return await exchange.fetchOrders(pair, since);
    } catch (e) {
      return e.message;
    }
  }

  async getMyOpenedOrdersForThePair(
    firstPairId,
    secondPairId,
    exchangeName,
    apiKey,
    secret
  ) {
    let exchangeId = exchangeName;
    let exchangeClass = ccxt[exchangeId];
    let exchange = new exchangeClass({
      apiKey: apiKey,
      secret: secret,
      timeout: 30000,
      enableRateLimit: true
    });
    let since = exchange.milliseconds() - 86400000 * 4;
    try {
      firstPairId = firstPairId.toString().toUpperCase();
      secondPairId = secondPairId.toString().toUpperCase();
      return await exchange.fetchOpenOrders(
        `${firstPairId}/${secondPairId}`,
        since
      );
    } catch (e) {
      return e.message;
    }
  }

  async closeOrderById(id, symbol, exchangeName, apiKey, secret) {
    let exchangeId = exchangeName;
    let exchangeClass = ccxt[exchangeId];
    let exchange = new exchangeClass({
      apiKey: apiKey,
      secret: secret,
      timeout: 30000,
      enableRateLimit: true
    });
    // return `Закрываем ордер по ID: ${id}`;
    return await exchange.cancelOrder(id, symbol);
  }
}

module.exports = Zenfuse;
